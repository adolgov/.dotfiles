set fileencoding=utf-8

" behaviour
set noeb vb t_vb=
set backspace=indent,eol,start
set nostartofline
set nobackup
set noswapfile
set mouse=a
" fix for tmux mouse
if &term =~ '^screen'
  " tmux knows the extended mouse mode
  set ttymouse=xterm2
endif

" appearance
set number
set ruler
set showmode

set autoindent
set expandtab
set shiftwidth=2
set softtabstop=2
set pastetoggle=<F2>

set scrolloff=4
set showmatch
set wildmenu
set autoread

set regexpengine=1
syntax enable

set ignorecase
set hlsearch
" set smartcase – fuck this

set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
  Plugin 'VundleVim/Vundle.vim'
  " status bar

  Plugin 'vim-airline/vim-airline'
  Plugin 'vim-airline/vim-airline-themes'

  " allows to move lines with ctrl+j/k
  Plugin 'matze/vim-move'

  " allows to use russian layout in cmd mode
  Plugin 'powerman/vim-plugin-ruscmd'

  " Snipmate and dependencies
  Plugin 'marcweber/vim-addon-mw-utils'
  Plugin 'tomtom/tlib_vim'
  Plugin 'garbas/vim-snipmate'
  Plugin 'honza/vim-snippets'

  " autocomplete
  Plugin 'ervandew/supertab'
  Plugin 'mattn/emmet-vim'

  " brakets magic
  Plugin 'jiangmiao/auto-pairs'
  Plugin 'luochen1990/rainbow'

  " comments magic
  Plugin 'tomtom/tcomment_vim'

  " unusable slow plugin so never install it again
  " Plugin 'Valloric/YouCompleteMe'
  Plugin 'gorodinskiy/vim-coloresque'

  " navigation
  " Plugin 'scrooloose/nerdtree'
  Plugin 'eiginn/netrw'
  Plugin 'wincent/command-t'
  Plugin 'rking/ag.vim'
  " Plugin 'taiansu/nerdtree-ag'
call vundle#end()

set t_Co=256
set laststatus=2
" disable Ex mode
nnoremap Q <nop>

let g:airline_theme='molokai'
let g:airline_detect_modified=1
let g:airline_inactive_collapse=1
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'

filetype on
filetype plugin on
filetype indent on
filetype plugin indent on

let g:netrw_banner = 0

" open each buffer in separate tab
:au BufAdd,BufNewFile * nested tab sball


" set splitright
set timeoutlen=1000 ttimeoutlen=0
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
let g:move_key_modifier = 'C'
let g:rainbow_active = 1
set clipboard=unnamed


" removing auto triming spaces on quit
fun! <SID>StripTrailingWhitespaces()
  let l = line(".")
  let c = col(".")
  %s/\s\+$//e
  call cursor(l, c)
endfun

" automatically clean trailing whitespaces on save
autocmd BufWritePre *.* :call <SID>StripTrailingWhitespaces()

" bind ctrl+S for saving file
" If the current buffer has never been saved, it will have no name,
" call the file browser to save it, otherwise just save it.
command -nargs=0 -bar Update if &modified
                           \|    if empty(bufname('%'))
                           \|        browse confirm write
                           \|    else
                           \|        confirm write
                           \|    endif
                           \|endif

nnoremap <silent> <C-S> :<C-u>Update<CR>
:inoremap <c-s> <c-o>:Update<CR>

" jumping to the beginngin and the end of line while editigin
inoremap <C-e> <C-o>$
inoremap <C-a> <C-o>0

