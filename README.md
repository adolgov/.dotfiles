# .dotfiles #

Dotfiles for myself and few ideas how to make comfortable OS from MacOS

### What is this repository for? ###

You could use my tricks, for more information read [wiki](https://bitbucket.org/adolgov/.dotfiles/wiki)

Cool stuff [here](https://github.com/skwp/dotfiles) but want to add:

* mouse support for tmux and vim
* key bindings for Ctrl + arrows/backspace; [Cmd+K for clear](https://coderwall.com/p/r6saiq/iterm2-tmux-cmd-k)
* remove vim stuff from command line
* buffer of mac os